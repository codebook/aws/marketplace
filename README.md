# AWS Marketplace

_tbd_




## References

* [AWS Marketplace](https://aws.amazon.com/marketplace/)
* [AWS Marketplace Documentation](https://aws.amazon.com/documentation/marketplace/)
* [AWS SaaS Seller Integration Guide](https://s3.amazonaws.com/awsmp-loadforms/SaaS%20Seller%20Integration%20Guide.pdf)
* [Amazon API Gateway Integration with AWS Marketplace](https://aws.amazon.com/about-aws/whats-new/2016/12/amazon-api-gateway-integration-with-aws-marketplace/)
* [Generate Your Own API Gateway Developer Portal](https://aws.amazon.com/blogs/compute/generate-your-own-api-gateway-developer-portal/)
* [A reference implementation that presents APIs on API Gateway as a catalog for 3rd party access](https://github.com/awslabs/aws-api-gateway-developer-portal)

